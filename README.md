![stability-work_in_progress](https://bitbucket.org/repo/ekyaeEE/images/477405737-stability_work_in_progress.png)
![internaluse-green](https://bitbucket.org/repo/ekyaeEE/images/3847436881-internal_use_stable.png)
![issues-open](https://bitbucket.org/repo/ekyaeEE/images/2944199103-issues_open.png)

# RATIONALE #
* This repo collects technological tools and human insights inside an internal effort to produce, generate audiovisual communications to the community
* This repo is a living document that will grow and adapt over time
![graphics](https://bitbucket.org/repo/48bkkAE/images/3536431956-sound_editor.jpg)
![exporting_session.jpg](https://bitbucket.org/repo/48bkkAE/images/1980281232-export_waves.jpg)

### What is this repository for? ###

* Quick summary
    - Strategies, insights and learnings in audio & visual communications
* Version 1.01

### How do I get set up? ###

* Summary of set up
    - In the beginning, you can check our _colophon_ (see below): lots of (open source) tools to convert analog data (_human voice_) to the digital realm
* Configuration
    - Check up [colophon.md](https://bitbucket.org/imhicihu/auvisual/src/master/Colophon.md)
* Dependencies
    -  Lots of [tools](https://bitbucket.org/imhicihu/auvisual/src/master/Colophon.md) needs lots of dependencies. There is no universal gobernance that applies to all
* Database configuration
    - There is no database to [CRUD](https://en.wikipedia.org/wiki/Create,_read,_update_and_delete), so, essentially verify our [Trello board](https://bitbucket.org/imhicihu/auvisual/addon/trello/trello-board)
* Deployment instructions
    - Check our [Buenas prácticas](https://bitbucket.org/imhicihu/auvisual/src/master/Buenas_practicas.md) y [Buenas prácticas en grabación de podcasts](https://bitbucket.org/imhicihu/auvisual/src/master/Buenas_practicas.md)
    - Check our [security & privacy issues](https://bitbucket.org/imhicihu/auvisual/src/master/security_privacy_issues.md)
![audacity.jpg](https://bitbucket.org/repo/48bkkAE/images/1684545415-1801032688-cartoon.jpg)
![podcast_producer.jpg](https://bitbucket.org/repo/48bkkAE/images/2429225702-gis.jpg)

### Source ###

* Check them on [here](https://bitbucket.org/imhicihu/auvisual/src)

### Issues ###

* Check them on [here](https://bitbucket.org/imhicihu/auvisual/issues)

### Changelog ###

* Please check the [Commits](https://bitbucket.org/imhicihu/auvisual/commits/) section for the current status

### Contribution guidelines ###

* Writing tests
    - There is no tests (at least) from inside this repository. Since all the material produced will be abroad this repo
* Code review
    - There is no code to examine or verify.
    - You can check our [Trello board](https://bitbucket.org/imhicihu/auvisual/addon/trello/trello-board) mentioned in a few paragraphs below

### Related repositories ###

* Some repositories linked with this project:
     - [Streaming](https://bitbucket.org/imhicihu/streaming/src/master)
     
### Who do I talk to? ###

* ~~Repo owner or admin~~
    ~~- Contact `imhicihu` at `gmail` dot `com`~~
* Other community or team contact
    - Contact is _enable_ on the [board](https://bitbucket.org/imhicihu/auvisual/addon/trello/trello-board) of this repo. (You need a [Trello](https://trello.com/) account)

### Code of Conduct

* Please, check our [Code of Conduct](https://bitbucket.org/imhicihu/auvisual/src/master/Code_of_conduct.md)

### Legal ###

* All trademarks are the property of their respective owners.

### License ###

* The content of this project itself is licensed under the ![MIT Licence](https://bitbucket.org/repo/ekyaeEE/images/2049852260-MIT-license-green.png)